import React, {Fragment, useContext} from "react";
import app from "./base";
import {AuthContext} from "./Auth";
import {Link} from "react-router-dom";

const Home = () => {
  const {currentUser} = useContext(AuthContext);
  return (
    <Fragment>
      <h1>Home</h1>
        {currentUser && <button onClick={() => app.auth().signOut()}>Sign out</button>}
        {!currentUser && <Link to={'/login'}>Sign In</Link>}
    </Fragment>
  );
};

export default Home;
